var Log = function(options) {
  var options = options || {};
  this.file = options.file || "af5-ais.log";
  this.level = options.level || ["error"];
  this.unique_name = typeof options.unique_name == "undefined" ? true : options.unique_name;
  this.fso = new ActiveXObject("Scripting.FileSystemObject");
  this.createLogFolder(this.fso.GetParentFolderName(options.file));
  this.createLogFile();
};

Log.prototype.createLogFolder = function(folder) {
  var parent_folder = this.fso.GetParentFolderName(folder);
  if (!this.fso.FolderExists(parent_folder)) {
    this.createLogFolder(parent_folder);
  }
  return this.fso.FolderExists(folder) ? folder : this.fso.CreateFolder(folder);
};

Log.prototype.createLogFile= function() {
  if (this.unique_name) {
    var d = new Date();
    this.file = this.fso.GetParentFolderName(this.file) + "\\" +
      d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() +
      " " + d.getHours() + "-" + d.getMinutes() + "-" +
      d.getSeconds() + " " + this.fso.GetFileName(this.file);
  }
  this.file = this.fso.CreateTextFile(this.file, true, false);
  this.add("������� ����� ������������� � �������� ������ ������");
};

Log.prototype.add = function(message, msg_type) {
  msg_type = msg_type || "info";
  for (var i = 0; i < this.level.length; i++) {
    var d = new Date();
    var timestamp = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() +
      " " + d.getHours() + ":" + d.getMinutes() + ":" +
      d.getSeconds() + ". ";
    var typestamp = msg_type.toUpperCase() + ": ";
    if (msg_type.toLowerCase() == this.level[i].toLowerCase()) {
      WScript.Echo(timestamp + typestamp + message);
      this.file.WriteLine(timestamp + typestamp + message);
      break;
    }
  }

};

Log.prototype.close = function() {
  this.add("����������");
  this.file.Close();
};