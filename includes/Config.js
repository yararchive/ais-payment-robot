var Config = {
  db: {
    AIS: {
      server: "SQLSERVER",
      name: "SQLDBNAME",
      user: "SQLDBUSER",
      pass: "SQLDBPASS"
    },
    robot_id: "00000000-0000-0000-0000-000000000000", // ID ������������ -- ������-����������� � ��
    role_id: "00000000-0000-0000-0000-000000000000", // ID ����, ����������� ���������� ���������� �� ����������
    max_reconnects: 2, // ������� ��� ���������� ������� ����������� � ��. 0 - ����� ������� �� ���������.
    reconnect_interval: 1 // � ��������,
  },
  log: {
    level: ["info", "error", "critical"],
    file: ".\\log\\request-robot.log",
    unique_name: true
  },
  mail: {
    smtpserver: "smtp.xxxxxxxxx.ru",
    smtpserverport: 465,
    smtpconnectiontimeout: 30,
    smtpusessl: true,
    sendusername: "from@xxxxxxxxx.ru",
    sendpassword: "1234567890",
    fromfield: "from@xxxxxxxxx.ru"
  }
};