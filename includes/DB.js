var DB = function(options) {
  var options = options || {};
  this.server = options.server || "";
  this.user = options.user || "";
  this.pass = options.pass || "";
  this.name = options.name || "";
  this.connect_tries = 0;
  this.max_reconnects = !options.max_reconnects && options.max_reconnects != 0 ? 5 : options.max_reconnects;
  // ��������� ���������� �������
  this.result = null;
};

DB.prototype.connect = function() {
  this.connection = new ActiveXObject("ADODB.Connection");
  try {
    this.connect_tries++;
    log.add("������� ����������� � " + this.connect_tries + " � �� " + this.name + " �� ������� " + this.server);
    this.connection.open("Provider=SQLOLEDB;Initial Catalog=" + this.name + ";uid=" + this.user + ";pwd=" + this.pass + ";Data Source=" + this.server);
    log.add("����������� ���������� � �� " + this.name + " �� ������� " + this.server);
    this.connect_tries = 0;
    this.connection.CommandTimeOut = 9000;
    this.connection.CommandTimeout = 9000;
  } catch(e) {
    log.add(e.name + " -> " + e.message + " ---> " + strQuery, "error");
    if (this.connect_tries < this.max_reconnects || this.max_reconnects == 0) {
      Util.sleep(Config.db.reconnect_interval * 1000);
      this.connect();
    } else {
      log.add("��������� ������� ���������� � �� " + this.name + " �� ������� " + this.server + ". ����������� ������ ����������.", "critical");
      log.close();
      WScript.Quit();
    }
  }
  return this;
};

DB.prototype.disconnect = function() {
  if (this.connection.State == 1) {
    this.connection.Close;
    log.add("���������� � �� " + this.name + " �� ������� " + this.server + " ���� �������.");
  }
  this.connection = null;
  return this;
};

DB.prototype.query = function(strQuery, options) {
  var options = options || {};
  var adOpenForwardOnly = options.adOpenForwardOnly || 0;
  var adLockReadOnly = options.adLockReadOnly || 1;
  var adCmdText = options.adCmdText || 1;

  try {
    this.connect_tries++;
    // �������� ���������� ��������� �������
    // this.clear();
    var result = new ActiveXObject("ADODB.Recordset");
    result.open(strQuery, this.connection, adOpenForwardOnly, adLockReadOnly, adCmdText);
    this.connect_tries = 0;
  } catch(e) {
    log.add(e.name + " -> " + e.message, "error");
    if (this.connect_tries < this.max_reconnects || this.max_reconnects == 0) {
      Util.sleep(Config.db.reconnect_interval * 1000);
      this.query(strQuery);
    } else {
      log.add("��������� ������� ���������� ������� " + strQuery.replace(/ +/gi, " ") + " � ���� ������ " + this.name + ". ����������� ������ ����������.", "critical");
      log.close();
      this.disconnect();
      WScript.Quit();
    }
  }
  return result;
};

// �������� ���������
DB.prototype.clear = function(rs) {
  if (rs) {
    rs.Close;
    rs = null;
  } else if (this.result !== null) {
    this.result.Close;
    this.result = null;
  }
};

// ��������� ������ � �� � ���������� ID
// ���� ������ ����, ������ ���������� ID
// �����:
// id_column - �������, � ������� �������� ID
// table - �������, �� ������� ������������� ������
// condition - �������, �� ������� ����������� ������������� ������
// values (������������� ������ ���� - ��������) - ������, ����������� � �������
// select_if_exists (���������� ������ �����) - �������, ������ �� ������� ����� �����������, ���� � �� ��� ���� ������.
//                    ID ����� �� ���������, ������������� ����������.
DB.prototype.insertOrId = function(options) {
  options = options || {};
  options.id_column = options.id_column || "Id";

  var res_keys = new Array();
  var res_values = new Array();

  for (key in options.values) {
    res_keys.push(key);
    res_values.push(options.values[key]);
  }

  var res = this.query("\
    IF NOT EXISTS (SELECT [" + options.id_column + "] FROM [" + this.name + "]." + options.table + " WHERE " + options.condition + ")\
    BEGIN\
      INSERT INTO [" + this.name + "]." + options.table + "\
        (" + res_keys + ")\
      OUTPUT Inserted." + options.id_column + "\
      VALUES (" + res_values + ")\
    END\
    ELSE\
    BEGIN\
      SELECT [" + options.id_column + "]" + (options.select_if_exists ? ", " + options.select_if_exists : "") + " FROM [" + this.name + "]." + options.table + " WHERE " + options.condition + "\
    END");

  var res_arr = {};

  // ������� ������������� ������ �� ����������
  res_arr[options.id_column] = String(res.Fields.Item(options.id_column));
  if (res.Fields.count > 1) {
    for (var i = 0; i < options.select_if_exists.length; i++)
      res_arr[options.select_if_exists[i]] = String(res.Fields.Item(options.select_if_exists[i]));
  }
  // TODO: ����������, ���� �������, ��� ������ ��������� ID.
  // ����� ���� ���������� ������� ������ ��� ������� - �������
  // ���������, � ����� ��� � ���������
  if (options.need_to_be_updated === true) {
    log.add("���������� ���������� � ������� " + options.table + " � ID " + res_arr[options.id_column]);
    var update_new_values = "";
    for (var i = 0; i < res_keys.length; i++) {
      update_new_values += res_keys[i] + " = " + res_values[i];
      if (i < res_keys.length - 1) update_new_values += ", ";
    }
    this.connection.execute("\
      UPDATE [" + this.name + "]." + options.table + " \
        SET " + update_new_values + " \
      WHERE " + options.id_column + " = " + res_arr[options.id_column]);
  }

  return res_arr;
};